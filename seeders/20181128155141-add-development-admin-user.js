module.exports = {
  up: queryInterface => queryInterface.bulkInsert('Users', [{
    firstName: 'Test',
    lastName: 'Admin',
    email: 'test@admin.com',
    password: 'password',
  }], {}),

  down: queryInterface => queryInterface.bulkDelete('Users', null, {}),
};
