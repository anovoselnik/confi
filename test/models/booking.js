const chai = require('chai');
const faker = require('faker');
const fixtures = require('../fixtures');
const truncate = require('../truncate');

describe('Booking model', async () => {
  const email = faker.internet.email();
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();
  const phone = faker.phone.phoneNumber();

  afterEach(async () => truncate());

  it('should validate firstName', async () => {
    try {
      const booking = await fixtures.createBooking({ lastName, phone, email });
      chai.expect(booking).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });


  it('should validate lastName', async () => {
    try {
      const booking = await fixtures.createBooking({ firstName, phone, email });
      chai.expect(booking).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });

  it('should validate email', async () => {
    try {
      const booking = await fixtures.createBooking({ lastName, phone, firstName });
      chai.expect(booking).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });

  it('should validate phone', async () => {
    try {
      const booking = await fixtures.createBooking({ lastName, firstName, email });
      chai.expect(booking).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });

  it('should have confirmation code generated after creation', () => fixtures.createBooking().then((booking) => {
    chai.expect(booking.confirmationCode).to.have.length(32);
  }));
});
