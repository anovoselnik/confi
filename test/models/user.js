const chai = require('chai');
const faker = require('faker');
const fixtures = require('../fixtures');
const truncate = require('../truncate');

describe('User model', () => {
  const email = faker.internet.email();
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();
  const password = faker.internet.password();

  afterEach(async () => truncate());

  it('should validate firstName', async () => {
    try {
      const user = await fixtures.createUser({ lastName, password, email });
      chai.expect(user).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });


  it('should validate lastName', async () => {
    try {
      const user = await fixtures.createUser({ firstName, password, email });
      chai.expect(user).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });

  it('should validate email', async () => {
    try {
      const user = await fixtures.createUser({ lastName, password, firstName });
      chai.expect(user).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });

  it('should validate password', async () => {
    try {
      const user = await fixtures.createUser({ lastName, firstName, email });
      chai.expect(user).to.not.exist();
    } catch (err) {
      chai.expect(err.name).to.equal('SequelizeValidationError');
    }
  });
});
