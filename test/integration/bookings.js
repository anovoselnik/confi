const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const should = chai.should();
const fixtures = require('../fixtures');
const truncate = require('../truncate');

const server = fixtures.getServer();

describe('Bookings API', async () => {
  afterEach(async () => truncate());

  describe('POST /api/bookings', async () => {
    const invalidBookingParams = {};
    const validBookingParams = fixtures.bookingPayload();

    it('should respond with 400 if missing params', async () => {
      const res = await chai.request(server)
        .post('/api/bookings')
        .send(invalidBookingParams);

      res.status.should.equal(400);
      return true;
    });

    it('should respond with 200', async () => {
      const res = await chai.request(server)
        .post('/api/bookings')
        .send(validBookingParams);

      res.status.should.equal(200);
      return true;
    });
  });

  describe('GET /api/bookings', async () => {
    let user;

    beforeEach(async () => {
      user = await fixtures.createUser();
      const booking1 = await fixtures.createBooking();
      const booking2 = await fixtures.createBooking();
    });

    it('should respond with 401 if invalid token', async () => {
      const invalidToken = 'invalidToken';
      const res = await chai.request(server)
        .get('/api/bookings')
        .set('Authorization', invalidToken);

      res.status.should.equal(401);
      return true;
    });

    it('should respond with bookings array if valid token', async () => {
      const validToken = fixtures.getToken(user);
      const res = await chai.request(server)
        .get('/api/bookings')
        .set('Authorization', validToken);

      res.status.should.equal(200);
      res.body.length.should.equal(2);
      return true;
    });
  });

  describe('DELETE /api/bookings', async () => {
    let user;
    let booking;

    beforeEach(async () => {
      user = await fixtures.createUser();
      booking = await fixtures.createBooking();
    });

    it('should respond with 401 if invalid token', async () => {
      const invalidToken = 'invalidToken';
      const res = await chai.request(server)
        .delete(`/api/bookings/${booking.id}`)
        .set('Authorization', invalidToken);

      res.status.should.equal(401);
      return true;
    });

    it('should respond with 404 if booking does not exist', async () => {
      const validToken = fixtures.getToken(user);
      const res = await chai.request(server)
        .delete('/api/bookings/-1')
        .set('Authorization', validToken);

      res.status.should.equal(404);
      return true;
    });

    it('should delete booking successfully if valid token', async () => {
      const validToken = fixtures.getToken(user);
      const res = await chai.request(server)
        .delete(`/api/bookings/${booking.id}`)
        .set('Authorization', validToken);

      res.status.should.equal(200);
      res.body.success.should.eq(true);
      return true;
    });
  });
});
