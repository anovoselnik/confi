const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const should = chai.should();
const fixtures = require('../fixtures');
const truncate = require('../truncate');

const server = fixtures.getServer();

describe('Users API', async () => {
  let user;

  beforeEach(async () => {
    user = await fixtures.createUser();
  });

  afterEach(async () => truncate());

  describe('POST /api/users/login', async () => {
    it('should respond with 400 if missing params', async () => {
      const invalidLoginParams = {
        email: user.email,
      };

      const res = await chai.request(server)
        .post('/api/users/login')
        .send(invalidLoginParams);

      res.status.should.equal(400);
      return true;
    });

    it('should respond with 404 if user does not exist', async () => {
      const invalidLoginParams = {
        email: 'wrongEmail',
        password: user.password,
      };

      const res = await chai.request(server)
        .post('/api/users/login')
        .send(invalidLoginParams);

      res.status.should.equal(404);
      return true;
    });

    it('should respond with access token if email and password are valid', async () => {
      const validLoginParams = {
        email: user.email,
        password: user.password,
      };

      const res = await chai.request(server)
        .post('/api/users/login')
        .send(validLoginParams);

      res.status.should.equal(200);
      res.body.token.should.be.a('string');
      return true;
    });
  });
});
