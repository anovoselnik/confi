const faker = require('faker');
const jwt = require('jsonwebtoken');

const models = require('../models');
const server = require('../index');
const config = require('../config/config.json');

const userPayload = () => ({
  email: faker.internet.email(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  password: faker.internet.password(),
});

const bookingPayload = () => ({
  email: faker.internet.email(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phone: faker.phone.phoneNumber(),
});

const createUser = async (userData) => {
  if (!userData) {
    userData = userPayload();
  }
  return models.User.create(userData)
    .then(user => user.toJSON());
};

const createBooking = (bookingData) => {
  if (!bookingData) {
    bookingData = bookingPayload();
  }
  return models.Booking.create(bookingData)
    .then(booking => booking.toJSON());
};

const getServer = () => server;

const getToken = (user) => {
  const token = jwt.sign({ email: user.email }, config.secret);
  return `Bearer ${token}`;
};

module.exports = {
  createUser,
  createBooking,
  userPayload,
  bookingPayload,
  getServer,
  getToken,
};
