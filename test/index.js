process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const should = chai.should();
const fixtures = require('./fixtures');

const server = fixtures.getServer();

require('./models/booking');
require('./models/user');
require('./integration/users');
require('./integration/bookings');

describe('Confi API', async () => {
  describe('GET /api', async () => {
    it('should respond with 200', async () => {
      const res = await chai.request(server)
        .get('/api');

      res.status.should.equal(200);
      res.type.should.equal('text/html');
      res.text.should.equal('Welcome to Confi API');
      return true;
    });
  });
});
