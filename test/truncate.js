const models = require('../models');

const truncate = async () => {
  const modelKeys = Object.keys(models);
  return Promise.all(
    modelKeys.map((key) => {
      if (['sequelize', 'Sequelize'].includes(key)) return null;
      return models[key].destroy({ where: {}, force: true });
    }),
  );
};

module.exports = truncate;
