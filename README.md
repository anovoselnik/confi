# Confi API
API docs: [Postman](https://documenter.getpostman.com/view/4158894/RzfcMBoH)

## Requirements
Node version: 10.13.0

MySQL database

## Setup
1. Clone repo
2. run ```npm install```
3. create local dev and test mysql databases
4. run ```cp config/config.json.example config/config.json```
5. edit config/config.json and enter local db credentials
6. run ```npm install -g sequelize-cli```
7. run ```sequelize db:migrate```
8. run ```sequelize db:seed:all```
9. run ```node index.js```

## Running tests
run ```npm test```