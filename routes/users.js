const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

const models = require('../models');

const router = express.Router();

router.post('/login', async (req, res) => {
  try {
    const data = req.body;
    if (
      !data
      || !data.email
      || !data.password
    ) {
      return res.status(400).send({
        message: 'Enter all the data',
      });
    }

    const user = await models.User.findOne({
      where: {
        email: data.email,
        password: data.password,
      },
    });

    if (!user) {
      return res.status(404).send({
        message: 'Not found',
      });
    }

    const token = jwt.sign({ email: user.email }, config.secret);

    return res.json({ token });
  } catch (err) {
    console.error(err);
    return res.status(500).send('Internal server error occured');
  }
});

module.exports = router;
