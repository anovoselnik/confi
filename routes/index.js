const express = require('express');

const bookingsRouter = require('./bookings');
const usersRouter = require('./users');

const router = express.Router();

router.get('/', (req, res) => res.send('Welcome to Confi API'));

router.use('/bookings', bookingsRouter);
router.use('/users', usersRouter);

module.exports = router;
