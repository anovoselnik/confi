const express = require('express');

const models = require('../models');
const authenticateUser = require('../middleware/authenticateUser');

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const validatedPayload = req.body;
    if (
      !validatedPayload
      || !validatedPayload.firstName
      || !validatedPayload.lastName
      || !validatedPayload.email
      || !validatedPayload.phone
    ) {
      return res.status(400).send({
        message: 'Enter all the data',
      });
    }

    const booking = await models.Booking.create(validatedPayload);
    return res.json(booking);
  } catch (err) {
    console.error(err);
    return res.status(500).send('Internal server error occured');
  }
});

router.get('/', authenticateUser, async (req, res) => {
  try {
    const bookings = await models.Booking.findAll();
    return res.json(bookings);
  } catch (err) {
    console.error(err);
    return res.status(500).send('Internal server error occured');
  }
});

router.delete('/:id', authenticateUser, async (req, res) => {
  const bookingId = req.params.id;
  try {
    const booking = await models.Booking.findByPk(bookingId);
    if (!booking) {
      return res.status(404).send('Not found');
    }
    await booking.destroy();
    return res.json({ success: true });
  } catch (err) {
    console.error(err);
    return res.status(500).send('Internal server error occured');
  }
});

module.exports = router;
