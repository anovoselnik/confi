const express = require('express');
const bodyParser = require('body-parser');

const apiRouter = require('./routes');

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.use('/api', apiRouter);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

module.exports = app;
