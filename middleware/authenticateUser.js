const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

function authenticateUser(req, res, next) {
  let token = req.headers.authorization;

  if (token && token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
  }

  if (token) {
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.status(401).send('Unauthorized');
      }
      // decoded = { email: 'userEmail' }
      // here you could find user by
      // email in db and set it to req
      // so you have current user in routes that authenticate user
      return next(null);
    });
  } else {
    return res.status(401).send('Unauthorized');
  }
}

module.exports = authenticateUser;
