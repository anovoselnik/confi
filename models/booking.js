const crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const Booking = sequelize.define('Booking', {
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    confirmationCode: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    hooks: {
      beforeValidate: (booking) => {
        const currentDate = (new Date()).valueOf().toString();
        const bookingCopy = booking;
        const confirmationCode = crypto.createHash('md5').update(booking.email + booking.phone + currentDate).digest('hex');
        bookingCopy.confirmationCode = confirmationCode;
        return bookingCopy;
      },
    },
  });
  return Booking;
};
