

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'Bookings',
    'confirmationCode',
    {
      type: Sequelize.STRING,
      allowNull: false,
    },
  ),

  down: queryInterface => queryInterface.removeColumn('Bookings', 'confirmationCode'),
};
